
## Terraform

Terraform code to create Kubernetes cluster in AWS (EKS)

- You need to have an account in AWS, installed aws cli.
- Edit terraform.tfvars file with your AWS region and cluster name
- Configure your AWS environment with the following command:

```
aws configure
aws sts get-session-token --serial-number  arn:aws:iam::156001095759:mfa/tatiana_maslova1@epam.com --token-code 

terraform init
terraform plan -var-file terraform.tfvars 
terraform apply -var-file terraform.tfvars
```

destroy:
```
terraform plan -destroy -var-file terraform.tfvars -out terraform.tfplan
terraform apply terraform.tfplan
```
