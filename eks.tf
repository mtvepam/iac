#################
# About EKSrole #
#################
data "aws_iam_role" "o_eks_role" {
  name = "eks_role"
}
data "aws_iam_role" "o_node_role" {
  name = "EKS_nodegroup_role"
}

#################
#     EKS       #
#################
resource "aws_eks_cluster" "eks_cluster" {
  name     = var.cluster_name
  role_arn = data.aws_iam_role.o_eks_role.arn

  vpc_config {
    subnet_ids  = data.aws_subnets.o_subnets.ids
    security_group_ids = [data.aws_security_group.o_sg_publicru.id, data.aws_security_group.o_sg_publiceu.id, data.aws_security_group.o_sg_default.id]
    endpoint_private_access = true
    endpoint_public_access = true
  }
 depends_on = [aws_cloudwatch_log_group.lg_eks_application]
  enabled_cluster_log_types = ["api", "audit", "controllerManager", "scheduler"]
  
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}
output "o_eks_arn" {
  description = "EKS cluster ARN"
  value = aws_eks_cluster.eks_cluster.arn
}
output "o_eks_endpoint" {
  description = "EKS cluster endpoint"
  value = aws_eks_cluster.eks_cluster.endpoint
}

#################
#     EKS node  #
#################
resource "aws_eks_node_group" "node" {
  cluster_name    = var.cluster_name
  node_group_name = "node_group1"
  node_role_arn   = data.aws_iam_role.o_node_role.arn
  subnet_ids      = data.aws_subnets.o_subnets.ids
  instance_types  = ["t3a.small"]
  capacity_type   = "SPOT"

  scaling_config {
    desired_size =  var.HPA_MIN
    max_size     =  var.HPA_MAX
    min_size     = var.HPA_MIN
  }
  
  depends_on   = [aws_eks_cluster.eks_cluster]
  
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}

output "o_node_arn" {
  description = "EKS node ARN"
  value = aws_eks_node_group.node.arn
}
output "o_node" {
  description = "EKS node name"
  value = aws_eks_node_group.node.node_group_name
}

#################
#     EKS others      #
#################
resource "aws_eks_addon" "vpccni" {
  depends_on   = [aws_eks_node_group.node]
  cluster_name = aws_eks_cluster.eks_cluster.name
  addon_name   = "vpc-cni"
}
resource "aws_eks_addon" "coredns" {
  depends_on   = [aws_eks_node_group.node]
  cluster_name = aws_eks_cluster.eks_cluster.name
  addon_name   = "coredns"
}
resource "aws_eks_addon" "kubeproxy" {
  depends_on   = [aws_eks_node_group.node]
  cluster_name = aws_eks_cluster.eks_cluster.name
  addon_name   = "kube-proxy"
}