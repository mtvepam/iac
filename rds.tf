#################
#     RDS       #
#################
resource "aws_db_instance" "rds" {
 depends_on = [
    aws_eks_node_group.node
  ]
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t2.micro"
  backup_retention_period = 0
  vpc_security_group_ids = ["${aws_eks_cluster.eks_cluster.vpc_config[0].cluster_security_group_id}"] 
  identifier           = "mysql"
  db_name              = var.dbname
  username             = var.rootname
  password             = var.rootpass
  port                 = var.port
  skip_final_snapshot  = var.skip_final_snapshot
  tags                 = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}
output "o_rds_arn" {
  description = "ARN of DataBase"
  value = aws_db_instance.rds.arn
}
output "o_rds_endpoint" {
  description = "Information about created DataBase - IPaddress PORT"
  value = aws_db_instance.rds.endpoint  
}
output "o_rds_status" {
  description = "DataBase status"
  value = aws_db_instance.rds.status
}