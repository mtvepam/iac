variable "aws_region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_session_token" {}

variable "HPA_MIN" {
  type    = number
}
variable "HPA_MAX" {
  type    = number
}


#################
# vars for EKS
#################
variable "cluster_name" {
  type    = string
  default = "appbe-eks"
}

#################
# vars for RDS
#################
variable "dbname" {
  type        = string
  default     = "Weather"
}
variable "rootname" {
  type        = string
  default     = "root" 
}
variable "rootpass" {
  type        = string
   default     = "rootroot" 
}
variable "username" {
  type        = string
  default     = "workuser" 
}
variable "userpass" {
  type        = string
   default     = "workwork" 
}
variable "port" {
  type        = string
  default     = "3306"
}
variable "skip_final_snapshot" {
  type        = bool
  default     = true
}
variable "publicly_accessible" {
  type        = bool
  default     = false
}
