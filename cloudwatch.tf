#################
#   CloudWatch  #
#################
resource "aws_cloudwatch_log_group" "lg_eks_application" {
  name = "/aws/containerinsights/${var.cluster_name}/application"
  retention_in_days = 7
  tags = {
     owner = "tatiana_maslova1@epam.com"
     project = "AppBE"
   }
}
resource "aws_cloudwatch_log_group" "lg_eks_host" {
  name = "/aws/containerinsights/${var.cluster_name}/host"
  retention_in_days = 7
  tags = {
     owner = "tatiana_maslova1@epam.com"
     project = "AppBE"
   }
}
resource "aws_cloudwatch_log_group" "lg_eks_performance" {
  name = "/aws/containerinsights/${var.cluster_name}/performance"
  retention_in_days = 7
  tags = {
     owner = "tatiana_maslova1@epam.com"
     project = "AppBE"
   }
}
resource "aws_cloudwatch_log_group" "lg_rds_err" {
  name = "/aws/rds/instance/rds/error"
  retention_in_days = 7
  tags = {
     owner = "tatiana_maslova1@epam.com"
     project = "AppBE"
   }
}
resource "aws_cloudwatch_dashboard" "appeb-dashboard" {
  dashboard_name = "AppBE"
    dashboard_body = <<EOF
{
    "widgets": [
                {
            "type": "metric",
            "x": 0,
            "y": 8,
            "width": 8,
            "height": 4,
            "properties": {
                "metrics": [
                    [ { "expression": "METRICS()/5", "label": "Request", "id": "e1" } ],
                    [ "AWS/ApplicationELB", "RequestCount", "TargetGroup", "${aws_lb_target_group.appbe-main-tg.arn_suffix}", "LoadBalancer", "${aws_lb.appbe-alb.arn_suffix}", { "id": "m1", "visible": false, "yAxis": "left", "label": "Rate" } ]
                ],
                "view": "timeSeries",
                "stacked": false,
                "region": "${var.aws_region}",
                "stat": "Average",
                "period": 300,
                "title": "MAIN: Request rate"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 8,
            "height": 4,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/ApplicationELB", "HTTPCode_Target_5XX_Count", "TargetGroup", "${aws_lb_target_group.appbe-main-tg.arn_suffix}", "LoadBalancer", "${aws_lb.appbe-alb.arn_suffix}" ]
                ],
                "region": "${var.aws_region}",
                "title": "MAIN: 5xx errors"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 4,
            "width": 8,
            "height": 4,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/ApplicationELB", "TargetResponseTime", "TargetGroup", "${aws_lb_target_group.appbe-main-tg.arn_suffix}", "LoadBalancer", "${aws_lb.appbe-alb.arn_suffix}" ]
                ],
                "region": "${var.aws_region}",
                "title": "MAIN: Latency"
            }
        },
        {
            "type": "metric",
            "x": 16,
            "y": 6,
            "width": 8,
            "height": 6,
            "properties": {
                "metrics": [
                    [ "AWS/ApplicationELB", "RejectedConnectionCount", "LoadBalancer", "${aws_lb.appbe-alb.arn_suffix}" ]
                ],
                "view": "timeSeries",
                "stacked": false,
                "region": "${var.aws_region}",
                "title": "ALB: Rejected requests",
                "period": 300,
                "stat": "Sum"
            }
        },        
        {
            "type": "metric",
            "x": 8,
            "y": 8,
            "width": 8,
            "height": 4,
            "properties": {
                "metrics": [
                    [ { "expression": "METRICS()/5", "label": "Request", "id": "e1", "region": "${var.aws_region}" } ],
                    [ "AWS/ApplicationELB", "RequestCount", "TargetGroup", "${aws_lb_target_group.appbe-raw-tg.arn_suffix}", "LoadBalancer", "${aws_lb.appbe-alb.arn_suffix}", { "id": "m1", "visible": false, "yAxis": "left", "label": "Rate" } ]
                ],
                "view": "timeSeries",
                "stacked": false,
                "region": "${var.aws_region}",
                "stat": "Average",
                "period": 300,
                "title": "RAW: request rate"
            }
        }        
    ]
}
EOF
}

