#################
# common blocks #
#################
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">=3.20.0"
    }    
  }
   required_version = ">= 0.14"
}
provider "aws" {
  region = "${var.aws_region}"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  token = "${var.aws_session_token}"
}
#################
#   About  VPC  #
#################
data "aws_vpc" "o_vpc" {
  default = true
}
output "o_vpc_id" {
  description = "ID of the only VPC"
  value = data.aws_vpc.o_vpc.id  
}
#################
# About subnets #
#################
data "aws_subnets" "o_subnets" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.o_vpc.id]
  }
}
data "aws_subnet" "o_subnet" {
  for_each = toset(data.aws_subnets.o_subnets.ids)
  id = each.value
}
output "subnet_id" {
  description = "ID for every subnet"
  value = [for s in data.aws_subnet.o_subnet : s.id]
}
#################
# About SGs     #
#################
data "aws_security_group" "o_sg_publicru" {
    vpc_id = data.aws_vpc.o_vpc.id
    name   = "epam-by-ru"    
  }
data "aws_security_group" "o_sg_publiceu" {
    vpc_id = data.aws_vpc.o_vpc.id
    name   = "epam-europe"    
  }
data "aws_security_group" "o_sg_default" {
    vpc_id = data.aws_vpc.o_vpc.id
    name   = "default"    
  } 
