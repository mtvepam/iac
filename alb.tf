#################
#     EKS  nodes      #
#################
data "aws_instances" "nodes" {
  depends_on = [
    aws_eks_node_group.node
  ]
  instance_tags = {
    "eks:cluster-name" = var.cluster_name
  }
}

#################
#     ALB       #
#################
resource "aws_lb" "appbe-alb" {
  depends_on = [
    aws_eks_node_group.node
  ]
  name = "appbe-alb"
  internal = false
  load_balancer_type = "application"
  security_groups  = [aws_eks_cluster.eks_cluster.vpc_config[0].cluster_security_group_id, data.aws_security_group.o_sg_publicru.id, data.aws_security_group.o_sg_publiceu.id]
  subnets  = data.aws_subnets.o_subnets.ids
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}

#################
# for main env  #
#################
resource "aws_lb_target_group" "appbe-main-tg" {
  depends_on = [
    aws_eks_node_group.node
  ]
  name  = "appbe-main-tg"
  port = 32080
  protocol = "HTTP"
  vpc_id = data.aws_vpc.o_vpc.id
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}

resource "aws_lb_target_group_attachment" "appbe-main-tgattach" {
  depends_on = [
    aws_eks_node_group.node
  ]
  count = var.HPA_MIN
  target_group_arn = aws_lb_target_group.appbe-main-tg.arn
  target_id = data.aws_instances.nodes.ids[count.index]
  port = 32080
}

resource "aws_lb_listener" "appbe-main-listener" {
  depends_on = [
    aws_eks_node_group.node
  ]
  load_balancer_arn = aws_lb.appbe-alb.arn
  port = "80"
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.appbe-main-tg.arn
  }
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}

#################
# for other env #
#################
resource "aws_lb_target_group" "appbe-raw-tg" {
  depends_on = [
    aws_eks_node_group.node
  ]
  name = "appbe-raw-tg"
  port = 31080
  protocol = "HTTP"
  vpc_id = data.aws_vpc.o_vpc.id
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}

resource "aws_lb_target_group_attachment" "appbe-raw-tgattach" {
  depends_on = [
    aws_eks_node_group.node
  ]
  count = var.HPA_MIN
  target_group_arn = aws_lb_target_group.appbe-raw-tg.arn
  target_id = data.aws_instances.nodes.ids[count.index]
  port = 31080
}

resource "aws_lb_listener" "appbe-raw-listener" {
  depends_on = [
    aws_eks_node_group.node
  ]
  load_balancer_arn = aws_lb.appbe-alb.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.appbe-raw-tg.arn
  }
  tags = {
    owner = "tatiana_maslova1@epam.com"
    project = "AppBE"
  }
}
